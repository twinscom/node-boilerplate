/*jslint node, maxlen: 80 */

"use strict";

module.exports = function makeProcessor() {
    return function process() {
        return "Hello World\n";
    };
};
