# Put your use cases here

Use cases can include an arbitrary number of steps (functions) that will be
executed in a sequence as defined in `defineRoutes()`. Each step is defined in
a separate file that must export a function maker. If you need a more complex
behavior, you can export whatever you want and define a constructor function
for that step in `defineRoutes()`.

## Examples

### use-cases/upload-file

#### use-cases/upload-file/schema.js

```js
const Joi = require("joi");

module.exports = Joi
    .object({
        name: Joi.string().trim().required(),
        path: Joi.string().trim().required()
    })
    .required();
```

#### use-cases/upload-file/process.js

```js
module.exports = function makeProcessor(dependencies) {
    return function process(fileModel) {
        // Returns a promise:
        return dependencies.gateways.saveFileModel(fileModel);
    };
};
```

#### use-cases/upload-file/format.js

```js
module.exports = function makeFormatter(dependencies) {
    return function format(fileModel) {
        // Returns an object:
        return dependencies.humanizeFileSize(fileModel);
    };
};
```

### server.js

```js
const Joi = require("joi");
const {makeServer, defineRoutes} = require("@twinscom/helpers");

module.exports = function makeServerAndDefineRoutes({config}) {
    const server = makeServer(config.server);

    defineRoutes({
        useCaseMakerConfig: {
            sequence: ["schema", "process", "format"],
            constructors: {
                schema: (schema) => (requestModel) => Joi.attempt(
                    requestModel,
                    schema
                )
            },
            dependencies: {
                gateways: {
                    saveFileModel(fileModel) {
                        /* gateway implementation */
                    }
                },
                humanizeFileSize() {
                    /* humanizer implementation */
                }
            },
            pathToUseCases: `${process.cwd()}/src/use-cases/`
        },
        server,
        routes: [
            {
                method: "post",
                path: "/file",
                handlers: ["upload-file"],
                adaptRequest(req) {
                    return {
                        name: req.params.name,
                        path: req.headers.path
                    };
                },
                adaptResponse(body) {
                    return {
                        status: 201,
                        body
                    };
                }
            }
        ]
    });

    return server;
};

```
