/*jslint node, maxlen: 80 */

"use strict";

const {makeServer, defineRoutes} = require("@twinscom/helpers");

module.exports = function makeServerAndDefineRoutes() {
    const server = makeServer();

    defineRoutes({
        useCaseMakerConfig: {
            sequence: ["process"],
            constructors: {
                process: (makeProcessor) => makeProcessor()
            },
            pathToUseCases: `${process.cwd()}/src/use-cases/`
        },
        server,
        routes: [
            {
                method: "get",
                path: "/test",
                handlers: ["test"],
                adaptResponse(body) {
                    return {
                        headers: {
                            "Content-Type": "text/plain"
                        },
                        body
                    };
                }
            }
        ]
    });

    return server;
};
