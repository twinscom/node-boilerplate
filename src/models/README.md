# Put your models here

## Examples

### file.js

```js
/*jslint node, maxlen: 80 */

"use strict";

module.exports = Object.freeze({
    create(requestModel) {
        return {
            id: null,
            name: requestModel.name.trim(),
            size: requestModel.size,
            path: requestModel.path,
            createdAt: Date.now(),
            deletedAt: null,
            error: null
        };
    }
});
```

### errors.js
