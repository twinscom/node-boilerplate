# Put your configs here

- **default.json** - read-only default app config
- **local.json** - gitignored custom app config that will be recursively
  merged with default.json
- **local.template.json** - a template for a custom config file
