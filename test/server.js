/*jslint node, maxlen: 80 */

"use strict";

const {describe, it, after} = require("mocha");
const chai = require("chai");
const chaiHttp = require("chai-http");

const makeServer = require("../src/server");

const {expect} = chai;

chai.use(chaiHttp);

const httpCodes = {
    ok: 200
};

const server = makeServer();

describe("server", function describeServer() {
    after((done) => server.close(done));

    it(
        "should respond with \"Hello World\"",
        function testServer() {
            return chai
                .request(server)
                .get("/test")
                .then((res) => Promise.all([
                    expect(res).to.have.status(httpCodes.ok),
                    expect(res).to.have.header("Content-Type", "text/plain"),
                    expect(res.text).to.eql("Hello World\n")
                ]));
        }
    );
});
