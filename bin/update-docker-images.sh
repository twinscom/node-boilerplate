#!/bin/sh

set -eu

docker-compose pull

docker-compose build --pull

docker-compose up -d
