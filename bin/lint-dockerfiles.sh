#!/bin/sh

set -eu

docker pull lukasmartinelli/hadolint
docker pull redcoolbeans/dockerlint

hadolint() {
    docker run \
        --rm \
        -i \
        lukasmartinelli/hadolint \
        hadolint - < "$1"
}

dockerlint() {
    docker run \
        --rm \
        -t \
        -v "$1:/Dockerfile:ro" \
        redcoolbeans/dockerlint
}

find "$PWD/dockerfiles/" -type f -name '*Dockerfile*' | while read -r file; do
    printf '\nLinting %s\n\n' "$file"
    hadolint "$file"
    dockerlint "$file"
done
