#!/bin/sh

set -eu

docker pull twinscom/sqlint

# shellcheck disable=SC2046

docker run \
    --rm \
    -v "$PWD:/mnt/:ro" \
    twinscom/sqlint \
    $(find src/ test/ dockerfiles/ -type f -name '*.sql')
