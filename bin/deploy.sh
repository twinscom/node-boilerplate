#!/bin/sh

set -eu

git reset --hard

git fetch origin master

git checkout "$2"

OLD_CONTAINER_ID=$(docker ps -q -f name="$1_app")

docker-compose up -d --scale app=2

sleep 30

docker stop "$OLD_CONTAINER_ID"

docker-compose restart nginx

docker rm "$OLD_CONTAINER_ID"

docker-compose up -d --scale app=1
