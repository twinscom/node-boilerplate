#!/bin/sh

set -eu

apk add --no-cache openssh-client
mkdir -p ~/.ssh/
echo "$ID_RSA" > ~/.ssh/id_rsa
chmod 400 ~/.ssh/id_rsa
ssh-keyscan "$REMOTE_HOST" >> ~/.ssh/known_hosts
