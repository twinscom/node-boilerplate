#!/bin/sh

set -eu

docker pull koalaman/shellcheck

# shellcheck disable=SC2046

docker run \
    --rm \
    -v "$PWD:/mnt/:ro" \
    koalaman/shellcheck \
    $(find bin/ src/ test/ dockerfiles/ -type f -name '*.sh')
