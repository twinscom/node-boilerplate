#!/bin/sh

set -eu

docker pull mterron/sqlcheck

sqlcheck() {
    docker run \
        --rm \
        -v "$PWD:/mnt/:ro" \
        -w /mnt/ \
        mterron/sqlcheck \
        --color_mode \
        --verbose \
        --file_name "$1"
}

find src/ test/ dockerfiles/ -type f -name '*.sql' | while read -r file; do
    sqlcheck "$file";
done
