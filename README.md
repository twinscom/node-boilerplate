# Node.js Web App Boilerplate

[![build status](https://gitlab.com/twinscom/node-boilerplate/badges/master/build.svg)](https://gitlab.com/twinscom/node-boilerplate/builds)
[![coverage report](https://gitlab.com/twinscom/node-boilerplate/badges/master/coverage.svg)](https://gitlab.com/twinscom/node-boilerplate/builds)

## Services

- Node.js Web App: [http://localhost/test](http://localhost/test)
- Swagger UI: [http://localhost/docs/](http://localhost/docs/)
- RethinkDB Web UI: [http://localhost:81/](http://localhost:81/)
- Redis Commander Web UI: [http://localhost:82/](http://localhost:82/)

## Development

```sh
# Set admin password for RethinkDB and Redis Commander Web UIs
printf "admin:`openssl passwd -apr1`\n" > src/config/.htpasswd

# Create a docker-compose.override.yml file with defined ports for nginx
cp docker-compose.override.dev.template.yml docker-compose.override.yml

# Optionally create a src/config/local.json config for the app
# cp src/config/local.template.json src/config/local.json

# Start the services
docker-compose up -d

# Connect to the app's container
docker-compose exec app sh

# Run linters and tests
npm test

# Run linters only
npm run lint

# Run tests only
npm run mocha

# Run a specific test
npm run mocha test/server.js
```

## Production

```sh
# Set admin password for RethinkDB and Redis Commander Web UIs
printf "admin:`openssl passwd -apr1`\n" > src/config/.htpasswd

# Create the docker-compose.override.yml file with defined ports for nginx:
cp docker-compose.override.prod.template.yml docker-compose.override.yml

# Optionally create a src/config/local.json config for the app
# cp src/config/local.template.json src/config/local.json

# Start the services
docker-compose up -d
```

## License

[CC0-1.0](LICENSE)
